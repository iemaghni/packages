{
  :pkg ...
  :deps ...
  :dist ...
  :patches ...
}

sudo mount -o bind /dev ./dev
sudo mount -t proc none ./proc
sudo mount -t tmpfs none ./tmp
sudo mount -o bind /hpkg ./hpkg
sudo env -i HOME=/build "$(command -v chroot)" . /bin/sh

# TODO:
- replace musl crypt.h by libxcrypt
- replace SHA-512 yescrypt (https://bugs.archlinux.org/task/71393)

# Languages to add:
- Haskell https://gitlab.haskell.org/ghc/ghc
- Koka https://github.com/koka-lang/koka (Haskell)
- Cryptol https://github.com/GaloisInc/cryptol (Haskell)
- Scala Native https://github.com/scala-native/scala-native (Scala)
- Pony https://github.com/ponylang/ponyc
