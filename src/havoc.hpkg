(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)

(use ./libc)
(use ./libunwind)

(use ./libwayland)
(use ./libxkbcommon)

# libwayland
(use ./libffi)

(use ./wayland-protocols)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)

  (os/setenv "PATH" (join-pkg-paths ":" "/bin" [ clang lld llvm libwayland ]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def libwayland-inc (string (libwayland :path) "/include"))
  (def libxkbcommon-inc (string (libxkbcommon :path) "/include"))

  (def out-bin (string (p :out) "/bin"))
  (os/mkdir out-bin)

  (def dir (unpack2-src src))
  (os/mkdir "./build")
  (os/cd "./build")

  (def src-dir (string "../" dir "/tsm"))
  (def src-files
    [ "wcwidth.c" "shl-htable.c"
      "tsm-render.c" "tsm-screen.c" "tsm-selection.c"
      "tsm-unicode.c" "tsm-vte-charsets.c" "tsm-vte.c" ])

  (def info
    (map |
      [ (string src-dir "/" $)
        (string "./" (string/replace-all "/" "-" $) ".o") ]
      src-files))
  (each [in out] info
    (sh/$ clang -std=c99
      -c -fpic
      ;(flatten (p :cflags))
      -o ,out ,in
      -I ,libxkbcommon-inc))
  (sh/$ ar -rcs libtsm.a ;(map last info))

  (def src-dir (string "../" dir))

  (sh/$ wayland-scanner client-header
    (string
      (wayland-protocols :path)
      "/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml")
    "./xdg-shell.h")
  (sh/$ wayland-scanner private-code
    (string
      (wayland-protocols :path)
      "/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml")
    "./xdg-shell.c")
  (sh/$ wayland-scanner client-header
    (string src-dir "/gtk-primary-selection.xml")
    "./gtk-primary-selection.h")
  (sh/$ wayland-scanner private-code
    (string src-dir "/gtk-primary-selection.xml")
    "./gtk-primary-selection.c")

  (def src-files [ "glyph.c" "main.c" ])

  (def info
    (map |
      [ (string src-dir "/" $)
        (string "./" (string/replace-all "/" "-" $) ".o") ]
      src-files))
  (def info
    [ ;info
      [ "./gtk-primary-selection.c" "./gtk-primary-selection.o" ]
      [  "./xdg-shell.c" "./xdg-shell.o" ] ])
  (each [in out] info
    (sh/$ clang -std=c99
      -c -fpie
      ;(flatten (p :cflags))
      -D _POSIX_C_SOURCE=200809L
      -D "VERSION=\"0.3.0\""
      -o ,out ,in
      -I .
      -I ,libwayland-inc
      -I ,libxkbcommon-inc))

  (sh/$ clang
    -pie
    ;(flatten (p :ldflags))
    -o (string out-bin "/havoc")
    ;(map last info)
    -L . -l tsm
    -L (string (libffi :path) "/lib") -l ffi
    -L (string (libwayland :path) "/lib") -l wayland-client -l wayland-cursor
    -L (string (libxkbcommon :path) "/lib") -l xkbcommon))

(def- src
  (fetch
    :url "https://codeload.github.com/ii8/havoc/tar.gz/refs/tags/0.3.1"
    :hash "sha256:c4e18de62435a6338c8453d0b6e84b5f284dbd179f608ec74326b39e2cb36e87"))

(defpkg havoc :builder |(builder src))
