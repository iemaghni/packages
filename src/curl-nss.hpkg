(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./cmake)
(import ./samu)
(use ./sbase)
(import ./posix/patch)
(use ./posix/sh)
(use ./hostname)
(use ./lang/perl)

(use ./libc)
(use ./libunwind)

(use ./libcurl)
(use ./libmbedtls)
(use ./libnghttp2)
(use ./libzlib)
(use ./libzstd)
(use ./pls_die/libnspr)
(use ./pls_die/nss)

(def- patches
  [ (fetch
      :url "https://github.com/concatime/curl/commit/bcb6bdb0c354d1258b613ffdf1fedf2e4a83f997.diff"
      :hash "sha256:7c1c2630f86dc5ebdc874b61631590f4b9ee78bb298f0e9b9ee42a1a204e138c") ])

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin"
      [ clang lld llvm cmake sbase
        hostname perl ]))
  (os/setenv "C_INCLUDE_PATH"
    (string (libnspr :path) "/include/nspr:" (nss :path) "/include"))
  (os/setenv "LIBRARY_PATH"
    (join-pkg-paths ":" "/lib"
      [ libunwind
        libnspr nss ]))

  (def dir (unpack2-src curl-src))
  (patch/run patches :dir dir)
  # FIXME: upstream
  (rewrite (string dir "/tests/runtests.pl")
    |(string/replace "sh $CURLCONFIG" "$CURLCONFIG" $))
  (os/mkdir "./build")
  (os/cd "./build")

  (def rpath [nss])
  (array/push (p :ldflags)
    (string "-Wl,-rpath," (join-pkg-paths ":" "/lib" rpath)))

  (sh/$ cmake
    -G Ninja
    -D CMAKE_MAKE_PROGRAM= ^ (bin samu/samu)
    -D CMAKE_HOST_SYSTEM_PROCESSOR= ^ ((p :host) :cpu)
    -D CMAKE_INSTALL_PREFIX= ^ (p :out)
    -D CMAKE_INSTALL_LIBDIR=lib
    -D CMAKE_SUPPRESS_REGENERATION=TRUE
    -D CMAKE_FIND_PACKAGE_PREFER_CONFIG=TRUE

    -D BUILD_SHARED_LIBS= ^ (string (p :dyn))
    -D CMAKE_BUILD_TYPE=RelWithDebInfo
    -D CMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE
    -D CMAKE_POSITION_INDEPENDENT_CODE=TRUE
    -D CMAKE_C_FLAGS= ^ (string/join (flatten (p :cflags)) " ")
    -D CMAKE_EXE_LINKER_FLAGS= ^
      (string/join
        [ ;(flatten (p :ldflags))
          "-l" "certdb"
          "-l" "certhi"
          "-l" "cryptohi"
          "-l" "nss_static"
          "-l" "nssb"
          "-l" "nssdev"
          "-l" "nsspki"
          "-l" "nssutil"
          "-l" "pk11wrap"
          "-l" "pkixcertsel"
          "-l" "pkixchecker"
          "-l" "pkixcrlsel"
          "-l" "pkixmodule"
          "-l" "pkixparams"
          "-l" "pkixpki"
          "-l" "pkixresults"
          "-l" "pkixstore"
          "-l" "pkixsystem"
          "-l" "pkixtop"
          "-l" "pkixutil"
          "-l" "ssl"
          "-l" ":libnspr4.a"
          "-l" ":libplc4.a"
          "-l" ":libplds4.a" ] " ")

    -D CMAKE_PREFIX_PATH= ^ (join-pkg-paths ";" "" [ libnghttp2 libzlib libzstd ])
    -D CMAKE_INSTALL_RPATH_USE_LINK_PATH=ON

    -D CURL_USE_NSS=ON
    -D BUILD_CURL_EXE=ON
    # TODO: -D USE_LIBIDN2=ON
    -D USE_NGHTTP2=ON
    -D CURL_ZSTD=ON
    (string "../" dir))
  (samu/run :jobs (p :jobs))
  (samu/run :jobs (p :jobs) :targets ["testdeps"])
  # FIXME:
  #(samu/run :targets ["test-full"])
  (samu/run :targets ["install"]))

(defpkg curl-nss :builder |(builder curl-src))
