(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./sbase)
(use ./posix/awk)
(use ./posix/grep)
(use ./posix/m4)
(use ./posix/make)
(use ./posix/sed)
(use ./posix/sh)
(use ./autoconf)
(use ./automake)
(use ./toollib)

(use ./libc)
(use ./liblinux)
(use ./libunwind)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin"
      [ clang lld llvm
        sbase awk grep m4 make sed
        autoconf automake toollib ]))
  (os/setenv "C_INCLUDE_PATH" (join-pkg-paths ":" "/include" [liblinux]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def dir (unpack2-src src))
  (os/mkdir "./build")
  (os/cd "./build")

  (sh/$ autoupdate (string "../" dir "/configure.ac"))
  (sh/$ autoreconf -i
    -I (pkg-path "/share/aclocal" toollib)
    (string "../" dir))
  (sh/$ (string "../" dir "/configure")
    --build= ^ ((p :host))
    --disable-dependency-tracking
    --enable-shared= ^ (if (p :dyn) "yes" "no")
    --enable-static= ^ (if (p :dyn) "no" "yes")
    --prefix= ^ (p :out)
    --with-pic
    LD=lld
    CFLAGS= ^ (string/join (flatten (p :cflags)) " ")
    LDFLAGS= ^ (string/join (flatten (p :ldflags)) " "))
  (sh/$ make -j (p :jobs))
  (sh/$ make install))

# mirror of https://git.netfilter.org/libmnl
(def- src
  (fetch
    :url "https://salsa.debian.org/pkg-netfilter-team/pkg-libmnl/-/archive/upstream/1.0.4/pkg-libmnl-upstream-1.0.4.tar.gz"
    :hash "sha256:f12486eaab7341835e54621bf5ab5847f7b6cdf7bf6be4cd831f0f86c79f493f"))

(defpkg libmnl :builder |(builder src))
