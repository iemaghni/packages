(use ../prelude)

(import ./meson)
(import ./posix/patch)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./pkg-config)
(use ./lang/python)
(use ./sbase)
(use ./posix/sed)
(use ./posix/sh)

(use ./libc++)
(use ./libc++abi)
(use ./libunwind)

(use ./libelf)
(use ./libffi)
(use ./libpcre2)
(use ./libzlib)
#(use ./util-linux) # CAUSES A CIRCULAR DEP

# TODO: upstream
(def- patches
  [ # FIXME: use proper fix https://unix.stackexchange.com/a/494872
    #(fetch
    #  :url "https://gitlab.com/iemaghni/patches/-/raw/main/glib/0002-lld-fix.patch"
    #  :hash "sha256:61c853906af7cb6c16ff4d31d52b044ad448d04903f865a004ec489d1f851888")
       ])

(defn- builder
  [dists &keys p]
  (def p (set-p p))

  (sh-setup)
  (setup-env)
  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin" [clang lld llvm pkg-config python sbase]))
  (os/setenv "CPLUS_INCLUDE_PATH" (pkg-path "/include/c++/v1" libc++))
  (os/setenv "LIBRARY_PATH"
    (join-pkg-paths ":" "/lib" [libc++ libc++abi libunwind]))

  (def srcs (map unpack2-src dists))
  (os/rename (last srcs) (string (first srcs) "/subprojects/gvdb"))
  (def dir (first srcs))

  (patch/run patches :dir dir)
  (rewrite (string dir "/meson.build")
    |(string/replace "'sh'" "'sh', '/bin/sh'" $))
  # https://gitlab.gnome.org/GNOME/glib/-/issues/2740
  (rewrite (string dir "/gio/gio-launch-desktop.c")
    |(string/replace-all "static_assert" "//" $))

  (os/mkdir "./build")
  (os/cd "./build")

  (def deps [libelf libffi libpcre2 libzlib]) # util-linux

  (array/push (p :ldflags) "-Wl,-rpath,$ORIGIN/../lib")

  (meson/run p (string "../" dir) :pc-path deps)
  (content-cp "./meson-logs/meson-log.txt" (string (p :out) "/meson-log.txt"))

  (def add-rpath |(string/replace " -L" " -Wl,-rpath,${libdir} -L" $))
  (when (p :dyn)
    (map |(rewrite $ add-rpath)
      (dir-it (string (p :out) "/lib/pkgconfig"))))

  # libpcre
  #(def pc (string (p :out) "/lib/pkgconfig/glib-2.0.pc"))
  #(sh/$ (bin sed) -i "/Requires.private/d" ,pc)

  # zlib
  (def pc (string (p :out) "/lib/pkgconfig/gio-2.0.pc"))
  (sh/$ (bin sed) -i "/Requires.private/d" ,pc)

  # libffi
  (def pc (string (p :out) "/lib/pkgconfig/gobject-2.0.pc"))
  (sh/$ (bin sed) -i "/Requires.private/d" ,pc)
  )

(def- dists
  [ (fetch
      :url "https://gitlab.gnome.org/GNOME/glib/-/archive/2.73.3/glib-2.73.3.tar.gz"
      :hash "sha256:56ad77af16e444f444e1a54f5f8a959212f078b332120a44728b17943fc4c2c0")
    (fetch
      :url "https://gitlab.gnome.org/GNOME/gvdb/-/archive/0854af0fdb6d527a8d1999835ac2c5059976c210.tar.gz"
      :hash "sha256:08352e54e8216d9001820c627c62858585465b51dc557cc22f0f4770ed182ebd")
      ])

# When chromium is built with static glib, you get:
# [595386:595386:0626/150153.042137:ERROR:browser_main_loop.cc(269)] GLib-GObject: g_param_spec_pool_lookup: assertion 'pool != NULL' failed
# https://github.com/sdroege/gstreamer-rs/issues/60#issuecomment-343722408:
# > This warning usually only happens if you have two different versions of GLib in your process.
# In fact, you have two glib, the static one inside chrome binary and another one
# in gtk. To fix this, glib is always built dynamically.
# https://gitlab.freedesktop.org/gstreamer/gst-build/-/issues/133:
# > GLib is designed to have one copy per process
# https://stackoverflow.com/a/46204158:
# > so the global variable has several copies
# when building gobject-introspection:
# > can't resolve libraries to shared libraries: gmodule-2.0
# FIXME: some tests fail
(defpkg glib2 :builder |(builder dists :dyn true :tests :ignored))
