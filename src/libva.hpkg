(use ../prelude)

(import ./meson)

(use ./sbase) # setup-env

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./pkg-config)

(use ./liblinux)
(use ./libunwind)

(use ./libdrm)
(use ./libffi)
(use ./libwayland)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin" [clang lld llvm pkg-config]))
  (os/setenv "C_INCLUDE_PATH" (join-pkg-paths ":" "/include" [liblinux]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  # FIXME:
  (def deps [libdrm libffi libwayland])

  (def dir (unpack2-src src))
  (os/mkdir "./build")
  (os/cd "./build")

  # for /lib/libva-*.so
  (array/push (p :ldflags) "-Wl,-rpath,$ORIGIN")

  # intel-media-driver/mesa require libva, but libva tries to load library built by them
  # or env var LIBVA_DRIVERS_PATH
  (meson/run p (string "../" dir)
    :pc-path deps :opts { :driverdir "/lib/dri" })

  (def add-rpath |(string/replace " -L" " -Wl,-rpath,${libdir} -L" $))
  (when (p :dyn)
    (map |(rewrite $ add-rpath)
      (dir-it (string (p :out) "/lib/pkgconfig"))))
  )

(def- src
  (fetch
    :url "https://codeload.github.com/intel/libva/tar.gz/refs/tags/2.15.0"
    :hash "sha256:869aaa9b9eccb1cde63e1c5b0ac0881cefc00156010bb49f6dce152471770ba8"))

# always shared
(defpkg libva :builder |(builder src { :dyn true }))
