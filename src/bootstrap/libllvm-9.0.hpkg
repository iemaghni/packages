(use ../../prelude)

(use ./clang-3.9)
(use ./cmake-3.12)
(use ./samu)
(use ./python)
(use ./patch)
(use ./seed)

(use ./libc++-3.9)
(use ./gcc-4.7) # FIXME: libgcc_s.so should be found directly by clang

(def llvm-9.0-src
  (fetch
    :url "https://codeload.github.com/llvm/llvm-project/tar.gz/llvmorg-9.0.1"
    :hash "sha256:be7b034641a5fda51ffca7f5d840b1a768737779f75f7c4fd18fe2d37820289a"))

(def- patches
  [ (fetch
      :url "https://github.com/concatime/llvm-project/commit/868afc62698a9cc0744340242461c7480e0b01be.patch"
      :hash "sha256:5c5129df054f467c064d5777f12365a9443e5fa2d4478b4cf52d596be940180f") ])

(defpkg libllvm-9.0
  :builder
  (fn []
    (os/symlink ;(map |(string $ "/lib/ld-musl-x86_64.so.1") [(seed :path) ""]))
    (os/symlink ;(map |(string $ "/bin/sh") [(seed :path) ""]))

    (os/setenv "PATH"
      (join-pkg-paths ":" "/bin"
        [clang-3.9 cmake-3.12 samu python patch seed]))
    (os/setenv "CPLUS_INCLUDE_PATH"
      (string (libc++-3.9 :path) "/include/c++/v1"))
    (os/setenv "LIBRARY_PATH"
      (string
        (join-pkg-paths ":" "/lib" [libc++-3.9])
        ":"
        (join-pkg-paths ":" "/lib64" [gcc-4.7])
        ))

    (def dir (unpack2-src llvm-9.0-src))
    (each i (map src-path patches) (sh/$ patch -d ,dir -i ,i -p 1))
    (os/mkdir "./build")
    (os/cd "./build")

    (sh/$ cmake
      -G Ninja
      -D CMAKE_BUILD_TYPE=Release
      -D CMAKE_INSTALL_PREFIX=

      # for libgcc_s.so.1
      -D CMAKE_INSTALL_RPATH= ^ (join-pkg-paths ":" "/lib64" [gcc-4.7])
      -D CMAKE_BUILD_WITH_INSTALL_RPATH=ON

      -D BUILD_SHARED_LIBS= ^ (string false)
      -D LLVM_HOST_TRIPLE=x86_64-unknown-linux-musl
      -D LLVM_TARGETS_TO_BUILD=host
      (string "../" dir "/llvm"))
    (sh/$ samu -j (dyn :parallelism))
    (sh/$ env DESTDIR= ^ (dyn :pkg-out) samu -j 1 install)))
