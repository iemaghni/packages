(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./cmake)
(import ./samu)
(use ./posix/sh)

(use ./libc)
(use ./libc++)
(use ./libc++abi)
(use ./libunwind)

(defn- builder
  [src &keys p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)
  (os/setenv "PATH" (join-pkg-paths ":" "/bin" [clang lld llvm cmake]))
  (os/setenv "CPLUS_INCLUDE_PATH" (pkg-path "/include/c++/v1" libc++))
  (os/setenv "LIBRARY_PATH"
    (join-pkg-paths ":" "/lib" [libc++ libc++abi libunwind]))

  (def dir (unpack2-src src))
  (os/mkdir "./build")
  (os/cd "./build")

  (sh/$ cmake
    -G Ninja
    -D CMAKE_MAKE_PROGRAM= ^ (bin samu/samu)
    -D CMAKE_HOST_SYSTEM_PROCESSOR= ^ ((p :host) :cpu)
    -D CMAKE_INSTALL_PREFIX= ^ (p :out)
    -D CMAKE_INSTALL_LIBDIR=lib
    -D CMAKE_SUPPRESS_REGENERATION=TRUE
    -D CMAKE_FIND_PACKAGE_PREFER_CONFIG=TRUE

    -D BUILD_SHARED_LIBS= ^ (string (p :dyn))
    -D CMAKE_BUILD_TYPE=Release
    -D CMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE
    -D CMAKE_POSITION_INDEPENDENT_CODE=TRUE
    -D CMAKE_C_FLAGS= ^ (string/join (flatten (p :cflags)) " ")
    -D CMAKE_CXX_FLAGS= ^ (string/join (flatten (p :cxxflags)) " ")
    -D CMAKE_EXE_LINKER_FLAGS= ^ (string/join (flatten (p :ldflags)) " ")
    (string "../" dir))
  (samu/run :jobs (p :jobs))
  (samu/run :targets ["install"])

  (def add-rpath |(string/replace " -L" " -Wl,-rpath,${libdir} -L" $))
  (when (p :dyn)
    (map |(rewrite $ add-rpath)
      (dir-it (string (p :out) "/lib/pkgconfig"))))
  )

(def- src
  (fetch
    :url "https://codeload.github.com/intel/gmmlib/tar.gz/refs/tags/intel-gmmlib-22.1.4"
    :hash "sha256:18f291b6d5c9a170468e050e301f23760bb5b20b79d28a49a791ace2f22880c9"))

(defpkg libigdgmm :builder |(builder src :dyn true))
