(use ../../prelude)

(use ../clang)
(use ../lld)
(use ../llvm)
(use ../lang/ocaml)
(use ../sbase)
(use ../posix/grep)
(import ../posix/patch)
(use ../posix/sed)
(use ../posix/sh)
(use ../deprecated/gmake)

(use ../libc)
(use ../libunwind)

(use ../libgmp)

(def- patches
  [ (fetch
      :url "https://patch-diff.githubusercontent.com/raw/ocaml/Zarith/pull/120.diff"
      :hash "sha256:db67c21d3b4a2bc755b7b17bcbecfeaa12b904b35f483797ca3bcf5ef6350021") ])

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin"
      [clang lld llvm ocaml sbase grep sed gmake]))
  (os/setenv "C_INCLUDE_PATH" (join-pkg-paths ":" "/include" [libgmp]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind libgmp]))

  (os/setenv "CFLAGS" (string/join (flatten (p :cflags)) " "))
  (os/setenv "LDFLAGS" (string/join (flatten (p :ldflags)) " "))

  (os/mkdir (string (p :out) "/lib"))
  (os/mkdir (string (p :out) "/lib/ocaml"))

  (os/cd (unpack2-src src))
  (os/setenv "CC" "clang")
  (patch/run patches)
  (rewrite "./project.mak"
    |(string/replace
      "$(OCAMLFIND) install" "$(OCAMLFIND) install -ldconf ignore" $))
  (sh/$ ./configure --installdir (p :out) ^ /lib/ocaml)
  (sh/$ gmake -j (p :jobs))
  (sh/$ gmake install)
  )

(def- src
  (fetch
    :url "https://codeload.github.com/ocaml/Zarith/tar.gz/refs/tags/release-1.12"
    :hash "sha256:cc32563c3845c86d0f609c86d83bf8607ef12354863d31d3bffc0dacf1ed2881"))

(defpkg ocaml-zarith :builder |(builder src))
