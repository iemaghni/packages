# WELCOME TO JANE STREET RABBIT HOLE
    (fetch
      :url "https://codeload.github.com/janestreet/base/tar.gz/refs/tags/v0.14.3"
      :hash "sha256:e34dc0dd052a386c84f5f67e71a90720dff76e0edd01f431604404bee86ebe5a")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_js_style/tar.gz/refs/tags/v0.14.1"
      :hash "sha256:3c5e1db934c3695315e00e27fd2df1f32efcb5c8f1c88b046b64b8548ccc04db")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_compare/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:9ecd7d68da862913b4ebc70d2e9f5d31baf45063b61a6e86a6f4a71182c5d525")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_sexp_conv/tar.gz/refs/tags/v0.14.3"
      :hash "sha256:2fc1f46e14016c93b0ba89a2af263e36a51957a53608751361f07ad3349c7639")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_hash/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:bfd7d82db037329239db5ea73b7ec08bb40979263d43858c649826408b66db2e")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_enumerate/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:75952f880fd65ab9c4f9c57295a96afcd36635435823e7484f856ef576c3fbba")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_cold/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:9f6479fc16dd8a7cf534058c60e3ba331628e095a06a5e4418159f6ed445647f")
    (fetch
      :url "https://codeload.github.com/janestreet/variantslib/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:bbd3f2f27a08c5a2954fcc50d0f14bee8a16241f16da597db0b914c01690b814")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_variants_conv/tar.gz/refs/tags/v0.14.2"
      :hash "sha256:2d668233089e90d20e893f0dadb39ae2e046b9c79c7b8bf950f8cb28e21ea619")
    (fetch
      :url "https://codeload.github.com/janestreet/typerep/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:f71d5094dbbaa9a0b70e68730cfa0140ca91962a69ac496673346d0af2d29648")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_typerep_conv/tar.gz/refs/tags/v0.14.2"
      :hash "sha256:1670dc267cacc8ea27a0b3aab4c08ed13fd622280e8169b93cea329def583abc")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_base/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:3c5073365367705742038d98284f9813d837efcf3d2172e22eecad166410dcf9")
    (fetch
      :url "https://codeload.github.com/janestreet/stdio/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:1685cb86b29b07075ba6028431cf4e3f687c071d89996a6437442db2dfe1b0b2")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_string/tar.gz/refs/tags/v0.14.1"
      :hash "sha256:9227b32ff2d7ec22e9deacd99253757df2291392a7540029a97cf745ef3a0ea9")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_here/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:9acc67b2a040181373f1db2da8913d873f01458a01e2d3e72d0c092e7abc6f90")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_sexp_value/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:7cf65018a3c5260a1ea1fb4f483bdb0a5269598a86e454a04e74ef5628f0b9bc")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_sexp_message/tar.gz/refs/tags/v0.14.1"
      :hash "sha256:4e625145e9cd6f89e3cecd3b14f6d68e0ac09ed24f629a407f9019685167a119")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_pipebang/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:0fc99c9c038d429692616fa0de73a50977981cc7c1eee404b3f559fdc55520e5")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_optcomp/tar.gz/refs/tags/v0.14.2"
      :hash "sha256:a52dd9a270bfbfd0aaf8f5ec75f3824184c6700e5bd3196c1aa6664df745ee05")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_optional/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:8a7db222cb6b6ca7b946890182d060edb2492912b29b36f74ef22b7c9d3c2be9")
    (fetch # latest tag does not compile
      :url "https://codeload.github.com/janestreet/ppx_assert/tar.gz/a165dc8f8f8754c8056cc923bd7f2d223332fc36"
      :hash "sha256:338e15ce57cf46036bd99ca90e1943c4d82e8f0ab29e116252920bbf3da208f6")
    (fetch
      :url "https://codeload.github.com/janestreet/jst-config/tar.gz/refs/tags/v0.14.1"
      :hash "sha256:a4cab8a69c62d440de8b59f75e56ce2bbaac2448c4b123e07b8b17b629d0b271")
    (fetch
      :url "https://codeload.github.com/janestreet/jane-street-headers/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:4413d2aa4a11bd360491e8b85c14cd799d07fa55c6ef3471b5a89bf11bf4c9cf")
    (fetch
      :url "https://codeload.github.com/janestreet/time_now/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:4fc6c0bfeb44c31bbc055456bb832ea782001b8708940f73c7c5f5cd744a2f8a")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_module_timer/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:860d86d71fc5a21f63c74b71ca86b6feeb14a1307d2594e02520dfab7d1df995")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_let/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:3cb879604cbee6cd9446d3dd26391680a39210e5dcbb98b654c2a6756f4eed90")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_inline_test/tar.gz/refs/tags/v0.14.1"
      :hash "sha256:d7e5c1b92e5ae1e9076979852c80cb192af443ff90e2fb11b5561df032aafb63")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_fixed_literal/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:1336d33092895fb9c5bf01588402db7131e4bb71b64ce96384bb2044f518d99a")
    (fetch
      :url "https://codeload.github.com/janestreet/fieldslib/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:64a740c40e38bc172670779c78a5088dd4f04fa9b599ba91ce2d511ff5b5cc56")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_fields_conv/tar.gz/refs/tags/v0.14.2"
      :hash "sha256:37b6967a03c2855427b4ff97e93f08162de520acb06acc97513f64526428ed64")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_custom_printf/tar.gz/refs/tags/v0.14.1"
      :hash "sha256:34cb58dbc886757ce95202059ee3e387a9581a0e00c36055d2c33ab8d764eb4f")
    (fetch
      :url "https://codeload.github.com/janestreet/bin_prot/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:b14cdda4050a6b899d901e946a17579fdd2ddd656acd0751fc6bf273c2c9ae2a")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_bin_prot/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:fecad38f08b09a95eed95e541e88574bc35b2c2b6047671461a7df7f45f91f8b")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_bench/tar.gz/refs/tags/v0.14.1"
      :hash "sha256:6eb8b9495d9d66ea4ebe0518b1cac022109aae4a3922464c27094dfd896323cb")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_stable/tar.gz/refs/tags/v0.14.1"
      :hash "sha256:0160ddd002fba9917c22dfaf29641c698ee8ebd4df52b76cd79a96b8338d88b7")
    (fetch
      :url "https://codeload.github.com/janestreet/splittable_random/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:02bf2f0f0482442cd573c0fbcf55ea89644466fe1f5e48c8c18018f58878f93a")
    (fetch
      :url "https://codeload.github.com/janestreet/base_quickcheck/tar.gz/refs/tags/v0.14.1"
      :hash "sha256:b69a18bf0149e3b553ada9af5258681000948bfb7b6646fee66aa462b507b058")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_expect/tar.gz/refs/tags/v0.14.2"
      :hash "sha256:c58afa94319f4c1675994e4a576a4f436675eea37b340d0ccddada6994a792bd")
    (fetch
      :url "https://codeload.github.com/janestreet/ppx_jane/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:fc0d1aaa9ff120eef1eeed5c2fcfb406b27c6ef865439dfeac38cce181d0c771")
    (fetch
      :url "https://codeload.github.com/janestreet/parsexp/tar.gz/refs/tags/v0.14.2"
      :hash "sha256:f6e17e4e08dcdce08a6372485a381dcdb3fda0f71b4506d7be982b87b5a1f230")
    (fetch
      :url "https://codeload.github.com/janestreet/sexplib/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:ad75ab155e2b4e2fec63fb178ef25a0a5a7de3834b939db95f38287dfd21cc68")
    (fetch
      :url "https://codeload.github.com/janestreet/base_bigstring/tar.gz/refs/tags/v0.14.0"
      :hash "sha256:646bf25b91f918d5f6bfea7857996c53cba19a85f432c685bea48653d2b0f9c8")
    (fetch
      :url "https://codeload.github.com/janestreet/core_kernel/tar.gz/refs/tags/v0.14.2"
      :hash "sha256:66f5353964d35a994ec7fdc88fe60ae5d497ac89a8042786f3e37d9e2202ce4b")
    #(fetch
    #  :url "https://codeload.github.com/janestreet/core/tar.gz/refs/tags/v0.14.1"
    #  :hash "sha256:8c158c12345d36a5cdd71081728f4317b2b04c09eb4126b6de00c482a6fec2a0")
