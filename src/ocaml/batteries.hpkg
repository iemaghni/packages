(use ../../prelude)

(use ../clang)
(use ../lld)
(use ../llvm)
(use ../lang/ocaml)
(use ../dune)
(use ../sbase)
(use ../posix/grep)
(use ../posix/sh)
(use ../deprecated/gmake)

(use ../libc)
(use ../libunwind)

(import ./_)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin"
      [clang lld llvm ocaml dune sbase grep sh gmake]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def deps [:num])
  (def dirs (map unpack2-src (_/get deps)))
  (os/setenv "OCAMLPATH" "/tmp/lib")
  (map |(defer (os/cd "..") (os/cd $) (print $ "...")
    (sh/$ dune build -j (p :jobs) @install)
    (sh/$ dune install --prefix=/tmp)) dirs)

  (os/cd (unpack2-src src))

  (os/mkdir (string (p :out) "/lib"))
  (os/mkdir (string (p :out) "/lib/ocaml"))
  (rewrite "./Makefile"
    |(string/replace "ocamlfind install"
      (string "ocamlfind install -destdir " (p :out) "/lib/ocaml") $))

  # TODO: detect if OCaml shared is built
  # Fatal error: unknown C primitive `unix_waitpid'
  # https://github.com/hackwaly/vscode-ocaml-debugger/issues/5#issuecomment-446330030
  (def d? true)

  (if d?
    (do
      (os/setenv "LD_LIBRARY_PATH" "/tmp/lib/stublibs")
      (os/symlink "../stublibs/dllnum_core_stubs.so"
        "/tmp/lib/num/dllnum_core_stubs.so"))
    (rewrite "./myocamlbuild.ml" |(string/replace "ocamlrun" "env" $)))


  (sh/$ gmake -j (p :jobs) BATTERIES_NATIVE_SHLIB= ^ (if d? "yes" "no"))
  (sh/$ gmake BATTERIES_NATIVE_SHLIB= ^ (if d? "yes" "no") install)
  )

(def- src
  (fetch
    :url "https://codeload.github.com/ocaml-batteries-team/batteries-included/tar.gz/refs/tags/v3.5.0"
    :hash "sha256:c2f59e2db4415e3d1b5e0abcf1cb0f940ea6d679097db15fc4904249932db7db"))

(defpkg ocaml-batteries :builder |(builder src))
