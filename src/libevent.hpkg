(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./cmake)
(import ./samu)
(use ./posix/sh)

(use ./libc)
(use ./libunwind)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH" (join-pkg-paths ":" "/bin" [clang lld llvm cmake]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def dir (unpack2-src src))
  (os/mkdir "./build")
  (os/cd "./build")

  (sh/$ cmake
    -G Ninja
    -D CMAKE_MAKE_PROGRAM= ^ (bin samu/samu)
    -D CMAKE_HOST_SYSTEM_PROCESSOR= ^ ((p :host) :cpu)
    -D CMAKE_INSTALL_PREFIX= ^ (p :out)
    -D CMAKE_INSTALL_LIBDIR=lib
    -D CMAKE_SUPPRESS_REGENERATION=TRUE
    -D CMAKE_FIND_PACKAGE_PREFER_CONFIG=TRUE

    -D BUILD_SHARED_LIBS= ^ (string (p :dyn))
    -D CMAKE_BUILD_TYPE=Release
    -D CMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE
    -D CMAKE_POSITION_INDEPENDENT_CODE=TRUE
    -D CMAKE_C_FLAGS= ^ (string/join (flatten (p :cflags)) " ")
    -D CMAKE_EXE_LINKER_FLAGS= ^ (string/join (flatten (p :ldflags)) " ")

    # disable=on, gg
    -D EVENT__DISABLE_OPENSSL=ON
    -D EVENT__LIBRARY_TYPE= ^ (if (p :dyn) "SHARED" "STATIC")
    (string "../" dir))
  (samu/run :jobs (p :jobs))
  (samu/run :targets ["install"]))

(def- src
  (fetch
    :url "https://codeload.github.com/libevent/libevent/tar.gz/refs/tags/release-2.1.12-stable"
    :hash "sha256:7180a979aaa7000e1264da484f712d403fcf7679b1e9212c4e3d09f5c93efc24"))

(defpkg libevent :builder |(builder src))
