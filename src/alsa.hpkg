(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./sbase)
(use ./posix/awk)
(use ./posix/find) # libtool
(use ./posix/grep)
(use ./posix/m4)
(use ./posix/make)
(use ./posix/sed)
(use ./posix/sh)
(use ./autoconf)
(use ./automake)
(use ./toollib)

(use ./libc)
(use ./liblinux)
(use ./libunwind)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin"
      [ clang lld llvm
        sbase awk find grep m4 make sed
        autoconf automake toollib ]))
  (os/setenv "C_INCLUDE_PATH" (join-pkg-paths ":" "/include" [liblinux]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (os/mkdir "./bin")
  (os/symlink (bin lld) "./bin/ld")
  (os/setenv "PATH" (string (os/getenv "PATH") ":" (os/cwd) "/bin"))

  (def dir (unpack2-src src))
  (os/mkdir "./build")
  (os/cd "build")

  (sh/$ autoupdate (string "../" dir "/configure.ac"))
  (sh/$ autoreconf -i
    -I (pkg-path "/share/aclocal" toollib)
    (string "../" dir))
  (sh/$ (string "../" dir "/configure")
    --build= ^ ((p :host))
    --disable-dependency-tracking
    --enable-shared= ^ (if (p :dyn) "yes" "no")
    --enable-static= ^ (if (p :dyn) "no" "yes")
    --prefix= ^ (p :out)
    --with-pic

    --disable-old-symbols
    --with-versioned= ^ (if (p :dyn) "yes" "no")
    # pipewire requires alsa, but alsa tries to load library built by pipewire
    # or env var ALSA_PLUGIN_DIR
    --with-plugindir=/lib/alsa-lib

    #LD=lld SEE ABOVE
    CFLAGS= ^ (string/join (flatten (p :cflags)) " ")
    LDFLAGS= ^ (string/join (flatten (p :ldflags)) " "))
  (sh/$ make -j (p :jobs))
  (sh/$ make install)

  (def add-rpath |(string/replace " -L" " -Wl,-rpath,${libdir} -L" $))
  (when (p :dyn)
    (map |(rewrite $ add-rpath)
      (dir-it (string (p :out) "/lib/pkgconfig"))))
  )

(def- src
  (fetch
    :url "https://codeload.github.com/alsa-project/alsa-lib/tar.gz/refs/tags/v1.2.5.1"
    :hash "sha256:bc1d9ed505e183fc59413425d34e8106322a0e399befcde8466bd96e6764e6c8"))

# chromium needs libasound.so.2, but alsa refuses to build both static and
# shared libs
(defpkg alsa :builder |(builder src { :dyn true }))
