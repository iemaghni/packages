(use ../../prelude)

(use ../sbase)
(use ../posix/grep)
(use ../posix/pax)
(use ../posix/sed)
(use ../pigz)
(use ../deprecated/bash)

(use ../libc)
(use ../libunwind)

(def- libgcc
  (fetch
    :url "http://dl-4.alpinelinux.org/alpine/v3.15/main/x86_64/libgcc-10.3.1_git20211027-r0.apk"
    :hash "sha256:85de9cdb9a580380da4f838765049808513842ef77ed94cd09becfd2083e61f6"))

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (os/setenv "PATH" (join-pkg-paths ":" "/bin" [sbase grep pax sed]))

  # Unsupported type 76 for ././@LongLink
  #(os/symlink "." "./rust-1.57.0-x86_64-unknown-linux-musl")
  #(unpack2-src src)
  #(os/rm "./rust-1.57.0-x86_64-unknown-linux-musl")

  (def tarball-path (src-path src))
  (print "unpacking " tarball-path)
  #(sh/$ (bin pigz) -dc ,tarball-path | pax -r -s ";^rust-*.*.*-x86_64-unknown-linux-musl/;;")
  (sh/$ (bin pigz) -dc ,tarball-path | pax -r)
  (os/cd (first (os/dir ".")))

  (sh/$ (bin bash) ./install.sh --prefix= ^ (p :out) --disable-ldconfig
    --components= ^ "rustc,cargo,rust-std-x86_64-unknown-linux-musl")

  (def tarball-path (src-path libgcc))
  (print "unpacking " tarball-path)
  (os/cd (p :out))
  (sh/$ (bin pigz) -dc ,tarball-path | pax -r -s ";^usr/;;")

  # is it fixable?
  (os/symlink (string (libunwind :path) "/lib/libunwind.a")
    "./lib/rustlib/x86_64-unknown-linux-musl/lib/libgcc_s.a")

  (os/rename "./bin/cargo" "./bin/carg0")
  (spit "./bin/cargo"
    (string "#!/bin/sh\n"
      "export CARGO_TARGET_X86_64_UNKNOWN_LINUX_MUSL_LINKER=lld\n"
      "export CARGO_TARGET_X86_64_UNKNOWN_LINUX_MUSL_RUSTFLAGS="
        "\"${CARGO_TARGET_X86_64_UNKNOWN_LINUX_MUSL_RUSTFLAGS} "
        "-C link-args=-L -C link-args=" (libc :path) "/lib\"\n"
      "exec env CC_x86_64_unknown_linux_musl=clang \"${0%?}0\" \"$@\"\n"))
  (os/chmod "./bin/cargo" 8r755)
  )

# from https://forge.rust-lang.org/infra/other-installation-methods.html#standalone-installers
(def- bin-x86_64
  (fetch
    :url "https://static.rust-lang.org/dist/rust-1.57.0-x86_64-unknown-linux-musl.tar.gz"
    :hash "sha256:56876ebca0e46236208c8bd3c3425dba553abe49639e1040ee8b95bc66a45d33"))

(defpkg rust-bin :builder |(builder bin-x86_64))
(defpkg rust :builder |(builder bin-x86_64))
