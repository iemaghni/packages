(use ../../prelude)

(use ../clang)
(use ../lld)
(use ../llvm)
(use ../cmake)
(import ../samu)
(use ../sbase)
(use ../posix/sh)

(use ../libc)
(use ../liblinux)
(use ../libunwind)

(use ../c/libexpat)
(use ../libffi)
(use ../libssl)
(use ../libxml2)
(use ../libzlib)

(def- python-cmake-src
  (fetch
    :url "https://codeload.github.com/python-cmake-buildsystem/python-cmake-buildsystem/tar.gz/56409629cf1fec75c7cf1a6209076594d3aa2290"
    :hash "sha256:ba538c5b4e27b8998fafccd465f60819d69be7644028ca536ef5db06b18ca6dd"))

# TODO: updates deps from 3.6 to 3.7

# python 3.6 bundles setuptools 40.6.2
# setuptools_scm>=5.0.0 needs setuptools>=42
# setuptools_scm>=6.0.0 needs setuptools>=45
# setuptools>=49 is problematic (https://github.com/pypa/setuptools/issues/2165)
(def- setuptools-src
  (fetch
    :url "https://raw.githubusercontent.com/python/cpython/v3.8.5/Lib/ensurepip/_bundled/setuptools-47.1.0-py3-none-any.whl"
    :hash "sha256:0e6ed68d292666370a4052af104cef4e027b4da823505b2a326f4dc1cb8eb269"))

# needed by importlib_resources
(def- setuptools_scm-src
  (fetch
    # FileNotFoundError: [Errno 2] No such file or directory: '/build/setuptools_scm-6.0.1/PKG-INFO'
    #:url "https://codeload.github.com/pypa/setuptools_scm/tar.gz/refs/tags/v6.0.1"
    #:hash "sha256:60ff6de2cb6b968d241ca9b5fa7d883bca080e1de692668cf94bd5175bdc42d1"
    :url "https://raw.githubusercontent.com/concatime/tar_gz-dist/master/setuptools_scm-6.0.1.tar.gz"
    :hash "sha256:41ba2b3d8d2d9abe2afc87db4c2916b9d0700026124e99e34435a795efe0cb81"))

# needed by importlib_resources
(def- toml-src
  (fetch
    :url "https://codeload.github.com/uiri/toml/tar.gz/refs/tags/0.10.2"
    :hash "sha256:71d4039bbdec91e3e7591ec5d6c943c58f9a2d17e5f6783acdc378f743fcdd2a"))

# needed by importlib_resources
(def- zipp-src
  (fetch
    # LookupError: setuptools-scm was unable to detect version for '/build/zipp-3.4.1'.
    # Make sure you're either building from a fully intact git repository or PyPI tarballs.
    # Most other sources (such as GitHub's tarballs, a git checkout without the .git folder)
    # don't contain the necessary metadata and will not work.
    #:url "https://codeload.github.com/jaraco/zipp/tar.gz/refs/tags/v3.4.1"
    #:hash "sha256:b13833926861546c9f2a10fe9d233ea3d05fac0b0448500a8e1de5f6e8c81aee"
    :url "https://raw.githubusercontent.com/concatime/tar_gz-dist/master/zipp-3.4.1.tar.gz"
    :hash "sha256:29d4bee90b6f2228a43bd0d3614bf2d757bdca90349479e9648148c4e2853801"))

# used by kitty
# importlib.resources is only available in python>=3.7
(def- importlib_resources-src
  (fetch
    # LookupError: setuptools-scm was unable to detect version for '/build/importlib_resources-5.1.2'.
    # Make sure you're either building from a fully intact git repository or PyPI tarballs.
    # Most other sources (such as GitHub's tarballs, a git checkout without the .git folder)
    # don't contain the necessary metadata and will not work.
    #:url "https://codeload.github.com/python/importlib_resources/tar.gz/refs/tags/v5.1.2"
    #:hash "sha256:fab3ac0c1372b41deb7e390b48ad4a5f78d0cfc3fb99f77ec089cd510475ac50"
    :url "https://raw.githubusercontent.com/concatime/tar_gz-dist/master/importlib_resources-5.1.2.tar.gz"
    :hash "sha256:9c87bd8ba7e21ec9fd4296cb5d1af91db13cb3a773e80e675ddbe7bb7dd3b2f2"))

# needed by importlib_metadata
# <4.0.0 because setup.py is missing
(def- typing_extensions-src
  (fetch
    :url "https://raw.githubusercontent.com/concatime/tar_gz-dist/master/typing_extensions-3.10.0.2.tar.gz"
    :hash "sha256:b160b7e0c68217118442b0d997a3c6bd3ff8bdeb4e67b6e3bd420d72d37259b5"))

# used by markdown
# <=4.8.3: https://github.com/python/importlib_metadata/commit/0019b0af43b9e381e2f0b14753d1bf40ce204490
(def- importlib_metadata-src
  (fetch
  # LookupError: setuptools-scm was unable to detect version
  # :url "https://codeload.github.com/python/importlib_metadata/tar.gz/refs/tags/v4.8.3"
  # :hash "sha256:5bf8a3b76970804a6b0db348792a098829fc36924ec10f7ed098cf6e734a4cf3"
    :url "https://raw.githubusercontent.com/concatime/tar_gz-dist/master/importlib_metadata-4.8.3.tar.gz"
    :hash "sha256:b7cc13fd051146d243d437f3c6b11487e0b49b385ca660262829f40a3c40591d"))

(def- wheel-src
  (fetch
    :url "https://codeload.github.com/pypa/wheel/tar.gz/refs/tags/0.37.1"
    :hash "sha256:a82516a039e521100ecdef137f9e44249bf6903f9aff7d368e84ac31d60597f5"))

# used by fish
(def- pexpect-src
  (fetch
    :url "https://codeload.github.com/pexpect/pexpect/tar.gz/refs/tags/4.8.0"
    :hash "sha256:f2ea54a12cc893636a60421e8da0a36c40caad7825eb674697f1e25f5f91245e"))

(def- pkgs-src
  [ setuptools_scm-src toml-src zipp-src importlib_resources-src
    typing_extensions-src importlib_metadata-src
    wheel-src
    pexpect-src ])

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin" [clang lld llvm cmake sbase]))
  (os/setenv "C_INCLUDE_PATH" (join-pkg-paths ":" "/include" [liblinux]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def python-dir (unpack2-src src))
  (os/symlink
    (string (setuptools-src :path) "/setuptools-47.1.0-py3-none-any.whl")
    (string python-dir "/Lib/ensurepip/_bundled/setuptools-47.1.0-py2.py3-none-any.whl"))
  (rewrite (string python-dir "/Lib/ensurepip/__init__.py")
    |(string/replace-all "40.6.2" "47.1.0" $))

  (def dir (unpack2-src python-cmake-src))
  (def libxml2-dir (unpack2-src libxml2-src))
  (def pkgs-dir (map unpack2-src pkgs-src))

  (os/mkdir "./build")
  (os/cd "./build")

  (def deps [libexpat libffi libssl libzlib])
  # TODO: https://fedoraproject.org/wiki/Changes/PythonNoSemanticInterpositionSpeedup
  (array/push (p :cflags) "-fno-semantic-interposition")

  (sh/$ cmake
    -G Ninja
    -D CMAKE_MAKE_PROGRAM= ^ (bin samu/samu)
    -D CMAKE_HOST_SYSTEM_PROCESSOR= ^ ((p :host) :cpu)
    -D CMAKE_INSTALL_PREFIX= ^ (p :out)
    -D CMAKE_INSTALL_LIBDIR=lib
    -D CMAKE_SUPPRESS_REGENERATION=TRUE
    -D CMAKE_FIND_PACKAGE_PREFER_CONFIG=TRUE

    -D BUILD_SHARED_LIBS= ^ (string (p :dyn))
    -D CMAKE_BUILD_TYPE=RelWithDebInfo # THIS PYTHON BUILD MAY BE SUBJECT TO CRASHES
    -D CMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE
    -D CMAKE_POSITION_INDEPENDENT_CODE=TRUE
    -D CMAKE_C_FLAGS= ^ (string/join (flatten (p :cflags)) " ")
    -D CMAKE_EXE_LINKER_FLAGS= ^ (string/join (flatten (p :ldflags)) " ")

    -D CMAKE_PREFIX_PATH= ^ (join-pkg-paths ";" "" deps)
    -D CMAKE_INSTALL_RPATH= ^ "$ORIGIN/../lib"
    -D CMAKE_INSTALL_RPATH_USE_LINK_PATH=ON

    -D BUILD_LIBPYTHON_SHARED= ^ (string (p :dyn))
    -D DOWNLOAD_SOURCES=OFF
    -D PYTHON_APPLY_PATCHES=OFF
    -D PYTHON_VERSION=3.7.12

    # for python3.6:
    # ModuleNotFoundError: No module named 'itertools'
    -D BUILTIN_ITERTOOLS=ON
    # ImportError: cannot import name 'reduce'
    -D BUILTIN_FUNCTOOLS=ON
    (string "../" dir))
  (samu/run :jobs (p :jobs))
  (samu/run :targets ["install"])

  # FIXME: pc python3.7 ~> python3.7m + lpthread in Libs

  (os/setenv "PATH" (string (os/getenv "PATH") ":" (p :out) "/bin" ))

  ################################## LIBXML2 #################################
  (clear-dir ".")
  (def src-dir (string "../" libxml2-dir "/python"))
  (def out-sitearch
    (sh/$<_ python -c
      (string
        "import distutils.sysconfig\n"
        "print(distutils.sysconfig.get_python_lib(True, False))\n")))
  (sh/$ python
    (string src-dir "/generator.py")
    (string src-dir "/../libxml2-api.xml")
    (string src-dir "/libxml2-python-api.xml"))
  (spit (string out-sitearch "/libxml2.py")
    (string
      (slurp (string src-dir "/libxml.py")) (slurp "./libxml2class.py")))
  (spit (string out-sitearch "/drv_libxml2.py")
    (slurp (string src-dir "/drv_libxml2.py")))
  (def src-files [ "libxml.c" "types.c" ])
  (def info
    (map |
      [ (string src-dir "/" $)
        (string "./" (string/replace-all "/" "-" $) ".o") ]
      src-files))
  (def info [ ;info [ "./libxml2-py.c" "./libxml2-py.o" ] ])
  (each [in out] info
    (sh/$ clang -std=c99
      -c -fpic
      ;(flatten (p :cflags))
      -o ,out ,in
      -I .
      -I ,src-dir
      -I (string (libxml2 :path) "/include/libxml2")
      -I (string (p :out) "/include/python3.7m")))
  (sh/$ clang
    -shared
    ;(flatten (p :ldflags))
    -o (string out-sitearch "/libxml2mod.so")
    ;(map last info)
    -L (string (libxml2 :path) "/lib") -l xml2
    -L (string (p :out) "/lib") -l python3.7m)
  ############################################################################

  # install setuptools by installing pip
  (sh/$ python -m ensurepip --altinstall)

  (def pkg-builder
    |(do
      (os/cd (string "../" $))
      (sh/$ python setup.py build -j (p :jobs))
      (sh/$ python setup.py install -O 1 --skip-build --prefix= ^ (p :out))
      ))
  (map pkg-builder pkgs-dir)
  )

(def- src
  (fetch
    :url "https://www.python.org/ftp/python/3.7.12/Python-3.7.12.tgz"
    :hash "sha256:33b4daaf831be19219659466d12645f87ecec6eb21d4d9f9711018a7b66cce46"))

(defpkg python :builder |(builder src))
