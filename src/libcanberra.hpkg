(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./sbase)
(use ./posix/awk)
(use ./posix/find) # libtool
(use ./posix/grep)
(use ./posix/m4)
(use ./posix/make)
(use ./posix/patch)
(use ./posix/sed)
(use ./posix/sh)
(use ./autoconf)
(use ./automake)
(use ./toollib)
(use ./pkg-config)

(use ./libc)
(use ./liblinux)
(use ./libunwind)

(use ./audio/libogg) # libvorbis
(use ./audio/libvorbis)
(use ./alsa)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin"
      [ clang lld llvm
        sbase awk find grep m4 make patch sed
        autoconf automake
        pkg-config toollib ]))
  (os/setenv "C_INCLUDE_PATH"
    (join-pkg-paths ":" "/include" [ liblinux toollib ]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [ libunwind toollib ]))

  (os/mkdir "./bin")
  (os/symlink (bin lld) "./bin/ld")
  (os/setenv "PATH" (string (os/getenv "PATH") ":" (os/cwd) "/bin"))

  (def dir (unpack2-src src))
  (spit (string dir "/gtkdoc/gtk-doc.make") "EXTRA_DIST =\n")
  (rewrite (string dir "/doc/Makefile.am")
    |(string/replace "endif\n" "endif\nREADME:\n	touch README\n" $))
  (rewrite (string dir "/configure.ac")
    |(string/replace "GTK_DOC_CHECK(1.9)\n" "" $))
  (os/mkdir "./build")
  (os/cd "./build")

  (def deps [libogg libvorbis alsa])

  # FIXME: breaks configure.ac
  #(sh/$ autoupdate (string "../" dir "/configure.ac"))
  (sh/$ autoreconf -i
    -I (pkg-path "/share/aclocal" toollib)
    -I (pkg-path "/share/aclocal" pkg-config)
    (string "../" dir))
  (sh/$ (string "../" dir "/configure")
    --build= ^ ((p :host))
    --disable-dependency-tracking
    --enable-shared= ^ (if (p :dyn) "yes" "no")
    --enable-static= ^ (if (p :dyn) "no" "yes")
    --prefix= ^ (p :out)
    --with-pic

    --with-builtin=alsa
    --sysconfdir=/etc
    --localstatedir=/var

    #LD=lld
    CFLAGS= ^ (string/join (flatten (p :cflags)) " ")
    LDFLAGS= ^ (string/join (flatten (p :ldflags)) " ")
    PKG_CONFIG_PATH= ^ (join-pkg-paths ":" "/lib/pkgconfig" deps)
    "PKG_CONFIG=pkg-config --static")
  (sh/$ make -j (p :jobs))
  (sh/$ make install))

# http://git.0pointer.net/libcanberra.git
(def- src
  (fetch
    :url "https://raw.githubusercontent.com/concatime/tar_gz-dist/master/libcanberra-git-v0.30.tar.gz"
    :hash "sha256:02ff0eb553f969975f126462203098249672fc4a65dedd0c690321f2ea4fbfac"))

(defpkg libcanberra :builder |(builder src))

(defpkg libcanberra-dyn :builder |(builder src { :dyn true }))
