(use ../prelude)

(use ./libc) # setup-ld

(use ./clang)
(use ./lld)
(use ./llvm)
(import ./samu)
(use ./posix/sh)
(use ./scdoc)

(use ./libunwind)

# TODO: use pkg-config instead of subproject

(def- pkgconf-src
  (fetch
    :url "https://codeload.github.com/pkgconf/pkgconf/tar.gz/1044bb57ca8a6e6679de63105ffabf6b8e8acfd7"
    :hash "sha256:f926b130517d7362cee926164408425adcc0f7c153459d0ae27c6bc4c38ce73a"))

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin" [clang lld llvm scdoc]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  # TODO: create virtual package with c99+ar+ld
  (os/setenv "PATH" (string (os/getenv "PATH") ":/bin"))
  (os/symlink (bin clang) "/bin/c99")
  #(spit "/bin/c99" "#!/bin/sh\nexec clang -x c -std=c99 \"$@\"\n")
  #(os/chmod "/bin/c99" 8r755)

  (array/push (p :cflags) "-O2")

  (def pkgconf-dir (unpack2-src pkgconf-src))
  (def dir (unpack2-src src))
  (os/symlink
    (string "../../" pkgconf-dir)
    (string "./" dir "/subprojects/pkgconf"))

  (defer (os/cd "..")
    (os/cd dir)
    # TODO: upstream
    (rewrite "./doc/meson.build"
      |(string/replace-all "'sh'" "'sh', '/bin/sh'" $)))

  (os/setenv "CC" "clang")

  (os/mkdir "./build_a")
  (os/mkdir "./build_b")
  (os/mkdir "./build_c")

  (os/cd "./build_a")
  # sadly, there is no automatic $rootdir in Ninja
  (rewrite (string "../" dir "/build.ninja")
    |(string/replace "r = .." (string "r = ../" dir) $))
  (sh/$ (bin samu/samu) -f ../ ^ ,dir ^ /build.ninja -j (p :jobs))

  (os/cd "../build_b")
  (sh/$ ../build_a/muon -C ../ ^ ,dir setup -D libpkgconf=disabled (os/cwd))
  (samu/run :jobs (p :jobs))

  (os/cd "../build_c")
  (sh/$ ../build_b/muon -C ../ ^ ,dir setup
    -D buildtype=debugoptimized
    -D prefix= ^ (p :out)

    -D pkgconf:tests=false
    -D pkgconf:prefix=/tmp # aka trash
    (os/cwd))
  (rewrite "./build.ninja" |(string/replace "rm -f $out && " "" $))
  (samu/run :jobs (p :jobs))
  (sh/$ ./muon install)
  )

(def- src
  (fetch
    :url "https://git.sr.ht/~iemaghni/muon/archive/ninja-bootstrap.tar.gz"
    :hash "sha256:ac0ff88fc7fb21686227306fb3edd52fde2bb2f55483a7643c91a3dc3587b416"))

(defpkg muon :builder |(builder src))

# debugoptimized is a sane default (-O2 -g)
# release for compute-heavy programs (-O3 -D NDEBUG)
# minsize for programs part of the seed (-Os)
(defn setup
  [ p src &keys opts0 ]

  (setup-ld)

  (def opts @{
    :prefix (p :out)
    :sysconfdir "/etc"
    :localstatedir "/var"
  # :runstatedir "/run"

    :buildtype "debugoptimized"
    :default_library (if (p :dyn) "shared" "static")
    :wrap_mode "nodownload"

    :b_pie true
    :b_lto true
    :b_lto_mode "thin"
    :b_lto_threads (p :jobs)

    :c_args (string/join (flatten (p :cflags)) " ")
    :c_link_args (string/join (flatten (p :ldflags)) " ")
    :cpp_args (string/join (flatten (p :cxxflags)) " ")
    :cpp_link_args (string/join (flatten (p :ldflags)) " ")
  })

  (merge-into opts opts0)

  (def env (os/environ))
  (when (nil? (get env "CC")) (put env "CC" "clang"))
  (when (nil? (get env "CXX")) (put env "CXX" "clang++"))

  (def pc (opts :pkg_config_path))
  (unless (nil? pc)
    (def pc-path
      (string/join
        (filter os/lstat
          [ ;(map |(pkg-path "/lib/pkgconfig" $) pc)
            ;(map |(pkg-path "/share/pkgconfig" $) pc) ])
        ":"))
    (put opts :pkg_config_path pc-path)
    # FIXME:
    (put env "PKG_CONFIG_PATH" pc-path)
    )

  (def opts (map |(string (first $) "=" (last $)) (pairs opts)))

  (os/execute
    [ (bin muon) "-C" src "setup"
      ;(flatten (map |["-D" $] opts)) (os/cwd) ]
    :xe env)
  )

(defn test
  [ &keys { :suites suites } ]
  (default suites [])

  (setup-ld)

  (os/execute
    [(bin muon) "install" ;(flatten (map |["-s" $] suites))]
    :x)
  )

(defn install
  [ &keys { :destdir destdir } ]

  (setup-ld)

  (def env (os/environ))
  (unless (nil? destdir)
    (put env "DESTDIR" destdir))

  (os/execute [(bin muon) "install"] :xe env)
  )
