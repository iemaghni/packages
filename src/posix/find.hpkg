(use ../../prelude)

(use ../clang)
(use ../llvm)
(use ../lld)
(use ../sbase)
(use ./awk)
(use ./grep)
(use ./make)
(use ./sed)
(use ./sh)

(use ../libc)
(use ../libunwind)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin" [clang llvm lld sbase awk grep make sed]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def dir (unpack2-src src))
  (os/mkdir "./build")
  (os/cd "./build")

  (sh/$ (string "../" dir "/configure")
    --build= ^ ((p :host))
    --disable-dependency-tracking
    --enable-shared= ^ (if (p :dyn) "yes" "no")
    --enable-static= ^ (if (p :dyn) "no" "yes")
    --prefix= ^ (p :out)
    --with-pic
    LD=lld
    CFLAGS= ^ (string/join (flatten (p :cflags)) " ")
    LDFLAGS= ^ (string/join (flatten (p :ldflags)) " "))
  (sh/$ make -j (p :jobs))
  (sh/$ make -C find install-binPROGRAMS))

(use ../../gnu-src)

(defpkg find :builder |(builder findutils-src))
