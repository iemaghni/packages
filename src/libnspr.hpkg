(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./sbase)
(use ./posix/awk)
(use ./posix/grep)
(use ./posix/m4)
(use ./posix/make)
(use ./posix/sed)
(use ./autoconf)
(use ./automake)
(use ./toollib)
(use ./lang/python)

(use ./libc)
(use ./libunwind)

(def- nspr-src
  (fetch
    :url "https://hg.mozilla.org/projects/nspr/archive/NSPR_4_30_RTM.zip"
    :hash "sha256:42fcc2270594bf4ffe158da6bfe5cf47a6d92f1fdacac1f60bb691ff19d3bbf8"))

(defpkg libnspr
  :builder
  (fn []
    (setup-ld)
    (sh-setup)

    (os/setenv "PATH"
      (join-pkg-paths ":" "/bin"
        [ clang lld llvm
          sbase awk grep m4 make sed
          autoconf automake toollib
          python ]))
    (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

    (def tarball (last (os/dir (nspr-src :path))))
    (def tarball-path (string (nspr-src :path) "/" tarball))
    (print "unpacking " tarball-path)
    (sh/$ python -m zipfile -e ,tarball-path .)

    (def dir
      (string "nspr-"
        (string/join (array/slice (string/split "." tarball) 0 -2) ".")))
    #(os/symlink "./configure.in" (string dir "/configure.ac"))
    (os/cd dir)
    (os/mkdir "./build")
    (sh/$ mv "./configure.in" "./configure.ac")
    (os/cd "..")
    (os/mkdir "./build")
    (os/cd "./build")

    (os/cd (string "../" dir))
    (sh/$ autoreconf -i
      -I (pkg-path "/share/aclocal" toollib))
    (sh/$ (string "../" dir "/configure")
      --build= ^ ((p :host))
      --disable-dependency-tracking
      --enable-shared= ^ (if build-shared-alt "yes" "no")
      --enable-static= ^ (if build-shared-alt "no" "yes")
      --prefix= ^ (dyn :pkg-out)
      --with-pic
      CC=clang
      CFLAGS= ^ (string/join *default-cflags* " ")
      LDFLAGS= ^ (string/join *default-ldflags* " "))
    (sh/$ make -j (dyn :parallelism))
    (sh/$ make install)))
