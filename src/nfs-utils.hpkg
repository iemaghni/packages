(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./sbase)
(use ./posix/awk)
(use ./posix/find) # libtool
(use ./posix/grep)
(use ./posix/m4)
(use ./posix/make)
(use ./posix/sed)
(use ./posix/sh)
(use ./autoconf)
(use ./automake)
(use ./toollib)
(use ./pkg-config)
(use ./rpcsvc)
(use ./deprecated/gmake)

(use ./libc)
(use ./liblinux)
(use ./libunwind)

(use ./libdevmapper)
(use ./libevent)
(use ./libsqlite3)
(use ./libtirpc)
(use ./keyutils)
(use ./util-linux) # libblkid

(def- q-src
  (fetch
    :url "https://raw.githubusercontent.com/openbsd/src/8149cf72cd2fc23ffc4b5bb9ef225cc216215489/sys/sys/queue.h"
    :hash "sha256:2467348c92f2130ad4ffa70eb973c13326fedf08ca0658bed17f0138bc2795cc"))

(defn- builder
  [src &keys p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin"
      [ clang lld llvm
        sbase awk find grep m4 make sed
        autoconf automake toollib pkg-config ]))
  (os/setenv "C_INCLUDE_PATH"
    (join-pkg-paths ":" "/include"
      [ liblinux
        libdevmapper libevent libsqlite3 libtirpc keyutils util-linux ]))
  (os/setenv "LIBRARY_PATH"
    (join-pkg-paths ":" "/lib"
      [ libunwind
        libdevmapper libevent libsqlite3 libtirpc keyutils util-linux ]))

  (os/mkdir "./include")
  (os/mkdir "./include/sys")
  (spit "./include/sys/_null.h" "#include <stddef.h>\n")
  (os/symlink (src-path q-src) "./include/sys/queue.h")
  (os/setenv "C_INCLUDE_PATH" (string (os/getenv "C_INCLUDE_PATH") ":" (os/cwd) "/include"))

  (def dir (unpack2-src src))
  # FIXME: out-of-tree build is broken
  #(os/mkdir "./build")
  #(os/cd "./build")
  (os/cd dir)

  # Keep your hands away from /var/lib!
  (sh/$ sed -i "25,31d" ./Makefile.am)

  # TODO: use libcap_ng

  # TODO: use pkgconfig
  (def deps [libdevmapper libevent libsqlite3 libtirpc keyutils util-linux])

  # not needed if pkg-config was fucking used
  (def rpath [libdevmapper])
  (array/push (p :ldflags)
    (string "-Wl,-rpath," (join-pkg-paths ":" "/lib" rpath)))

  (array/push (p :cflags) "-Wno-error=format-nonliteral") # FFS

  (sh/$ autoupdate (string "../" dir "/configure.ac"))
  (sh/$ autoreconf -i
    -I (pkg-path "/share/aclocal" toollib)
    -I (pkg-path "/share/aclocal" pkg-config)
    (string "../" dir))
  (sh/$ (string "../" dir "/configure")
    --build= ^ ((p :host))
    --disable-dependency-tracking
    --enable-shared= ^ (if (p :dyn) "yes" "no")
    --enable-static= ^ (if (p :dyn) "no" "yes")
    --prefix= ^ (p :out)
    --with-pic

    # TODO: use RPCGEN=$(command -v rpcgen 2>/dev/null) + test "$?" -eq 0
    --with-rpcgen= ^ (pkg-path "/bin/rpcgen" rpcsvc)
    --enable-libmount-mount
    --disable-gss
    --disable-sbin-override
    LIBS=-lblkid # libmount depends on libblkid

    LD=lld
    CFLAGS= ^ (string/join (flatten (p :cflags)) " ")
    LDFLAGS= ^ (string/join (flatten (p :ldflags)) " ")
    PKG_CONFIG_PATH= ^ (join-pkg-paths ":" "/lib/pkgconfig" deps))
  (sh/$ make -j (p :jobs) MAKE= ^ (bin gmake))
  (sh/$ make MAKE= ^ (bin gmake) install))

(def- src
  (fetch
    :url "http://git.linux-nfs.org/?p=steved/nfs-utils.git;a=snapshot;h=refs/tags/nfs-utils-2-6-1"
    :hash "sha256:ec79a4d78b70a2a72a8199803064a741a1c9e33815d18eee003eef3b51aac194"))

(defpkg nfs-utils :builder |(builder src))
