(use ../../prelude)

(use ../clang)
(use ../lld)
(use ../llvm)
(use ../sbase)
(use ../posix/awk)
(use ../posix/grep)
(use ../posix/sed)
(use ../posix/sh)
(use ../deprecated/gmake)

(use ../libc)
(use ../libunwind)

(use ./libskarnet)
(use ./execline)
(use ./s6)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)
  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin"
      [clang lld llvm gmake sbase awk grep sed]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def dir (unpack2-src src))
  (os/mkdir "./build")
  #(os/cd "./build") FIXME
  (os/cd dir)

  # FIXME: upstream
  (sh/$ mv ./Makefile ./GNUmakefile)

  (os/setenv "CFLAGS" (string/join (flatten (p :cflags)) " "))
  (os/setenv "LDFLAGS" (string/join (flatten (p :ldflags)) " "))

  # fake configure
  (sh/$ ./configure
    --enable-shared= ^ (if (p :dyn) "yes" "no")
    --enable-static= ^ (if (p :dyn) "no" "yes")
    --libdir= ^ (string (p :out) "/lib")
    --prefix= ^ (p :out)

    --with-include= ^ (string (libskarnet :path) "/include")
    --with-include= ^ (string (execline :path) "/include")
    --with-include= ^ (string (s6 :path) "/include")
    --with-lib= ^ (string (libskarnet :path) "/lib")
    --with-lib= ^ (string (execline :path) "/lib")
    --with-lib= ^ (string (s6 :path) "/lib")
    --with-sysdeps= ^ (string (libskarnet :path) "/lib/skalibs/sysdeps"))
  (sh/$ gmake -j (p :jobs))
  (sh/$ gmake install))

(def- src
  (fetch
  # :url "https://git.skarnet.org/cgi-bin/cgit.cgi/s6-rc/snapshot/s6-rc-0.5.3.2.tar.gz"
    :url "https://codeload.github.com/skarnet/s6-rc/tar.gz/refs/tags/v0.5.3.2"
    :hash "sha256:48881ee55c1c0d87fad438a81a73467a61002d1089a321e609d75a0ee390f138"))

(defpkg s6-rc :builder |(builder src))
