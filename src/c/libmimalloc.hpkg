(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./cmake)
(import ./samu)
(use ./posix/sh)

(use ./libc)
(use ./libunwind)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH" (join-pkg-paths ":" "/bin" [clang lld llvm cmake]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def dir (unpack2-src src))
  (rewrite (string dir "/CMakeLists.txt")
    |(string/replace "C CXX" "C" $))
  (os/mkdir "./build")
  (os/cd "./build")

  (sh/$ cmake
    -G Ninja
    -D CMAKE_MAKE_PROGRAM= ^ (bin samu/samu)
    -D CMAKE_HOST_SYSTEM_PROCESSOR= ^ ((p :host) :cpu)
    -D CMAKE_INSTALL_PREFIX= ^ (p :out)
    -D CMAKE_INSTALL_LIBDIR=lib
    -D CMAKE_SUPPRESS_REGENERATION=TRUE
    -D CMAKE_FIND_PACKAGE_PREFER_CONFIG=TRUE

    -D BUILD_SHARED_LIBS= ^ (string (p :dyn))
    -D CMAKE_BUILD_TYPE=Release
    -D CMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE
    -D CMAKE_POSITION_INDEPENDENT_CODE=TRUE
    -D CMAKE_C_FLAGS= ^ (string/join (flatten (p :cflags)) " ")
    -D CMAKE_EXE_LINKER_FLAGS= ^ (string/join (flatten (p :ldflags)) " ")
    (string "../" dir))
  (samu/run :jobs (p :jobs))
  (sh/$ ctest -j (p :jobs) --output-on-failure)
  (samu/run :targets ["install"]))

(def- src
  (fetch
    :url "https://codeload.github.com/microsoft/mimalloc/tar.gz/refs/tags/v2.0.5"
    :hash "sha256:fb000a017c289ddc0df749f16fef854e033e496276d8426bdd066b59c476f6cf"))

# always shared
(defpkg libmimalloc :builder |(builder src { :dyn true }))
