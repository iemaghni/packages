(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(import ./meson)
(use ./pkg-config)

(use ./libunwind)

(use ./libjson_c)
(use ./glib2)
(use ./gtk4)

# gtk4
(use ./libepoxy)
(use ./libgraphene)
(use ./libcairo)
(use ./libpango)
(use ./libwayland)
(use ./libxkbcommon)
(use ./gdk-pixbuf)

# libcairo
(use ./image/libpng)
(use ./libfreetype2)
(use ./libglvnd)
(use ./libpixman)
(use ./libzlib)
(use ./fontconfig)

# libpango
(use ./libfribidi)
(use ./libharfbuzz)

# libwayland
(use ./libffi)

# fontconfig
(use ./c/libexpat)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin" [clang lld llvm pkg-config glib2]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def dir (unpack2-src src))
  (rewrite (string dir "/meson.build") |(string/replace "+-3.0" "4" $))
  (os/mkdir "./build")
  (os/cd "./build")

  (def deps
    [ libjson_c glib2 gtk4
      # gtk4
      libepoxy libgraphene gdk-pixbuf libcairo libpango libwayland libxkbcommon
      # libcairo
      libfreetype2 libglvnd libpixman libpng libzlib fontconfig
      # libpango
      libfribidi libharfbuzz
      # libwayland
      libffi
      # fontconfig
      libexpat ])

  (meson/run p (string "../" dir) :pc-path deps))

(def- src
  (fetch
    :url "https://git.pwmt.org/pwmt/girara/-/archive/0.3.7/girara-0.3.7.tar.gz"
    :hash "sha256:41342213f8e745258f1db28cbb6ccc27a63009a5e001bf791bbe01ce436d4db7"))

(defpkg girara :builder |(builder src))
