(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./sbase)
(use ./posix/awk)
(use ./posix/grep)
(use ./posix/m4)
(use ./posix/make)
(use ./posix/sed)
(use ./posix/sh)
(use ./autoconf)
(use ./automake)
(use ./toollib)

(use ./libc)
(use ./liblinux)
(use ./libunwind)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)

  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin"
      [ clang lld llvm
        sbase awk grep m4 make sed
        autoconf automake toollib ]))
  (os/setenv "C_INCLUDE_PATH" (join-pkg-paths ":" "/include" [liblinux]))
  (os/setenv "LIBRARY_PATH" (join-pkg-paths ":" "/lib" [libunwind]))

  (def dir (unpack2-src src))
  # FIXME: upstream
  (rewrite (string "./" dir "/Makefile.am")
    |(string/replace
      "AM_CPPFLAGS ="
      "AM_CPPFLAGS = -D 'SYSCONFDIR=\"$(sysconfdir)\"' -D 'RUNSTATEDIR=\"$(runstatedir)\"' -D 'LOCALSTATEDIR=\"$(localstatedir)\"'" $))
  (rewrite (string "./" dir "/acpid.h")
    |(string/replace-all "\"/etc" "SYSCONFDIR \"" $))
  (rewrite (string "./" dir "/acpid.h")
    |(string/replace-all "\"/var/run" "RUNSTATEDIR \"" $))
  (rewrite (string "./" dir "/acpid.h")
    |(string/replace-all "\"/var" "LOCALSTATEDIR \"" $))
  (os/mkdir "./build")
  (os/cd "./build")

  #(sh/$ autoupdate (string "../" dir "/configure.ac"))
  (sh/$ autoreconf -i
    -I (pkg-path "/share/aclocal" toollib)
    (string "../" dir))
  (sh/$ (string "../" dir "/configure")
    --build= ^ ((p :host))
    --disable-dependency-tracking
    --enable-shared= ^ (if (p :dyn) "yes" "no")
    --enable-static= ^ (if (p :dyn) "no" "yes")
    --prefix= ^ (p :out)
    --with-pic

    --sysconfdir=/etc
    --runstatedir=/run
    --localstatedir=/var

    LD=lld
    CFLAGS= ^ (string/join (flatten (p :cflags)) " ")
    LDFLAGS= ^ (string/join (flatten (p :ldflags)) " "))
  (sh/$ make -j (p :jobs))
  (sh/$ make install))

(def- src
  (fetch
  # git://git.code.sf.net/p/acpid2/code
  # :url (sourceforge "acpid2/acpid-2.0.33.tar.xz")
    :url "https://salsa.debian.org/debian/acpid/-/archive/upstream/2.0.33/acpid-upstream-2.0.33.tar.gz"
    :hash "sha256:c210fa6a73a9ff555f9c434871ce616695924369cced7d56d9ade65661530cdd"))

(defpkg acpid :builder |(builder src))
