(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(use ./cmake)
(use ./pkg-config)
(import ./samu)
(use ./posix/sh)

(use ./libc)
(use ./libc++)
(use ./libc++abi)
(use ./liblinux)
(use ./libunwind)

(use ./libdrm)
(use ./libva)
(use ./libwayland)

# libdrm
(use ./libpciaccess)
# libwayland
(use ./libffi)

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (sh-setup)
  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin" [clang lld llvm cmake pkg-config]))
  (os/setenv "CPATH" (join-pkg-paths ":" "/include" [liblinux]))
  (os/setenv "CPLUS_INCLUDE_PATH" (pkg-path "/include/c++/v1" libc++))
  (os/setenv "LIBRARY_PATH"
    (join-pkg-paths ":" "/lib" [libc++ libc++abi libunwind]))

  (def dir (unpack2-src src))

  (rewrite (string dir "/builder/FindFunctions.cmake")
    |(string/replace-all "-lgcc" "-v" $))
  (rewrite (string dir "/builder/FindPackages.cmake")
    |(string/replace "--default-symver" "-v" $))

  # WHY THE FUCK IS RPATH EXPLICITELY DISABLED !?
  (rewrite (string dir "/builder/FindGlobals.cmake")
    |(string/replace "set( CMAKE_INSTALL_RPATH \"\" )\n" "" $))
  (rewrite (string dir "/builder/FindGlobals.cmake")
    |(string/replace "set( CMAKE_SKIP_BUILD_RPATH TRUE )\n" "" $))

  # not available in musl libc
  (rewrite (string dir "/tools/tracer/tracer/tracer_linux.cpp")
    |(string/replace-all "RTLD_DEEPBIND" "0U" $))

  (os/mkdir "./build")
  (os/cd "./build")

  (def deps
    [ libdrm libva libwayland
      # libdrm
      libpciaccess
      # libwayland
      libffi ])

  (sh/$ cmake
    -G Ninja
    -D CMAKE_MAKE_PROGRAM= ^ (bin samu/samu)
    -D CMAKE_HOST_SYSTEM_PROCESSOR= ^ ((p :host) :cpu)
    -D CMAKE_INSTALL_PREFIX= ^ (p :out)
    -D CMAKE_INSTALL_LIBDIR=lib
    -D CMAKE_SUPPRESS_REGENERATION=TRUE
    -D CMAKE_FIND_PACKAGE_PREFER_CONFIG=TRUE

    -D BUILD_SHARED_LIBS= ^ (string (p :dyn))
    -D CMAKE_BUILD_TYPE=Release
    -D CMAKE_INTERPROCEDURAL_OPTIMIZATION=TRUE
    -D CMAKE_POSITION_INDEPENDENT_CODE=TRUE
    -D CMAKE_C_FLAGS= ^ (string/join (flatten (p :cflags)) " ")
    -D CMAKE_CXX_FLAGS= ^ (string/join (flatten (p :cxxflags)) " ") # googletest
    -D CMAKE_EXE_LINKER_FLAGS= ^ (string/join (flatten (p :ldflags)) " ")

    -D CMAKE_PREFIX_PATH= ^ (join-pkg-paths ";" "" deps)

    -D BUILD_SAMPLES=OFF
    -D BUILD_TESTS=ON
    -D BUILD_TUTORIALS=OFF
    -D ENABLE_WAYLAND=ON
    #-D USE_SYSTEM_GTEST=ON
    (string "../" dir))
  (samu/run :jobs (p :jobs))
  (sh/$ ctest -j (p :jobs) --output-on-failure)
  (samu/run :targets ["install"])

  (def add-rpath |(string/replace " -L" " -Wl,-rpath,${libdir} -L" $))
  (when (p :dyn)
    (map |(rewrite $ add-rpath)
      (dir-it (string (p :out) "/lib/pkgconfig"))))
  )

(def- src
  (fetch
    :url "https://codeload.github.com/Intel-Media-SDK/MediaSDK/tar.gz/refs/tags/intel-mediasdk-21.3.0"
    :hash "sha256:f9b99237fa460cd8862beaad54fe033abf4e7606d05f42404293f86027e1f629"))

(defpkg libmfx :builder |(builder src { :dyn true }))
