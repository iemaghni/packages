(use ../prelude)

(use ./clang)
(use ./lld)
(use ./llvm)
(import ./gn)
(import ./samu)
(use ./sbase)
(use ./lang/python)

(use ./libc) # setup-ld
(use ./libc++)
(use ./libc++abi)
(use ./libunwind)

(use ./image/libjpeg)
(use ./image/libpng)
(use ./image/libwebp)
(use ./c/libexpat)
(use ./libfreetype2)
(use ./libglvnd)
(use ./libharfbuzz)
(use ./libzlib)
(use ./fontconfig)
(use ./icu)

# from ./DEPS
(def- skia-3p
  [ (fetch
    # hash is not consistent, see https://github.com/google/gitiles/issues/84
    # :url "https://android.googlesource.com/platform/external/dng_sdk/+archive/c8d0c9b1d16bfda56f15165d39e0ffa360a11123.tar.gz"
      :url "https://raw.githubusercontent.com/concatime/tar_gz-dist/master/dng_sdk-c8d0c9b1d16bfda56f15165d39e0ffa360a11123.tar.gz"
      :hash "sha256:501bc2e7a21fde2e04dfefac1b62f0036df6d0108ce76f43d93819918ce24630")
    (fetch
    # hash is not consistent, see https://github.com/google/gitiles/issues/84
    # :url "https://android.googlesource.com/platform/external/piex/+archive/bb217acdca1cc0c16b704669dd6f91a1b509c406.tar.gz"
      :url "https://raw.githubusercontent.com/concatime/tar_gz-dist/master/piex-bb217acdca1cc0c16b704669dd6f91a1b509c406.tar.gz"
      :hash "sha256:7fdc1826e47fad0e8ac7d49625841a33bcd614718691fcb786181926c74323c8")
    (fetch
    # hash is not consistent, see https://github.com/google/gitiles/issues/84
    # :url "https://chromium.googlesource.com/chromium/src/third_party/zlib/+archive/c876c8f87101c5a75f6014b0f832499afeb65b73.tar.gz"
      :url "https://raw.githubusercontent.com/concatime/tar_gz-dist/master/zlib-chromium-c876c8f87101c5a75f6014b0f832499afeb65b73.tar.gz"
      :hash "sha256:3f822fc97cf084ea04282918af43c6ec3e0f881601e5cbc809eedba3d17843f0") ])

(defn- builder
  [src &opt p]
  (def p (set-p p))

  (setup-ld)
  (os/setenv "PATH"
    (join-pkg-paths ":" "/bin" [clang lld llvm sbase python]))
  (os/setenv "CPATH"
    (join-pkg-paths ":" "/include"
      [ libexpat libfreetype2 libglvnd libharfbuzz libjpeg libpng libwebp libzlib
        fontconfig icu ]))
  (os/setenv "CPLUS_INCLUDE_PATH" (pkg-path "/include/c++/v1" libc++))
  (os/setenv "LIBRARY_PATH"
    (join-pkg-paths ":" "/lib"
      [ libc++ libc++abi libunwind
        libexpat libfreetype2 libglvnd libharfbuzz libjpeg libpng libwebp libzlib
        fontconfig icu ]))

  (def dir (unpack2-src src))
  (defer (os/cd "../../..")
    (def dir-3p (string dir "/third_party/externals"))
    (os/mkdir dir-3p)
    (os/cd dir-3p)
    (map |(os/rename ;$)
      (map |[ $ ;(array/slice (string/split "-" $) 0 1) ]
        (map unpack2-src skia-3p))))
  (os/mkdir "./build")
  (os/cd "./build")

  (array/push (p :cxxflags) ["-D" "EGL_NO_X11"])

  # FIXME: glibc vs musl
  # /third_party/externals/dng_sdk/source/dng_safe_arithmetic.h:118:45:
  #  error: cannot initialize a parameter of type 'long long *' with an rvalue of type 'std::int64_t *' (aka 'long *')
  #  if (__builtin_smulll_overflow(arg1, arg2, &result)) {
  (array/push (p :cxxflags) ["-D" "__WORDSIZE=64"])

  (gn/run (string "../" dir)
    :cc "clang"
    :cxx "clang++"
    :extra_cflags_c (flatten (p :cflags))
    :extra_cflags_cc (flatten (p :cxxflags))
    :is_component_build (p :dyn)
    :is_official_build true
    :skia_use_egl true
    :skia_use_vulkan true
    :skia_use_x11 false)
  (samu/run :jobs (p :jobs))
  (samu/run :targets ["install"]))

# hash is not consistent, see https://github.com/google/gitiles/issues/84
# curl -s https://skia.googlesource.com/skia/+archive/14b1d56a2b2b8f0ffcb62180382b7347673aaa11.tar.gz | sha256sum
# STUCK IN 14b1d56a2b2b8f0ffcb62180382b7347673aaa11
# DUE TO 2fa273eecf7c5291aa91cc081851323bed863409 AND 90006034c3d9fafcb1da3e52c615d4322abac4b2
(def- src
  (fetch
    :url "https://codeload.github.com/google/skia/tar.gz/14b1d56a2b2b8f0ffcb62180382b7347673aaa11"
    :hash "sha256:e99f7635269700004225bf7159968d4e015551a06d244ed99225521791a0647b"))

(defpkg libskia :builder |(builder src { :dyn true }))
